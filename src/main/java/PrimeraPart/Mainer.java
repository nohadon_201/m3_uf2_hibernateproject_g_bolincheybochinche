package PrimeraPart;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Mainer {
	public static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		SetInitalMonsters();
		SetAllPlayers();
		ArrayList<Monstre> monstresPartida = MonstreDAO.getInstance().llistarMonstresPartida(PartidaDAO.getInstance().get(1));
		
		Partida p =PartidaDAO.getInstance().get(1);
		p.setTorn(monstresPartida.get(0).getIdMonstre());
		PartidaDAO.getInstance().Update(p);
		
		while(MonstreDAO.getInstance().MonstreViu(p)==null) {
			Monstre m =MonstreDAO.getInstance().get(PartidaDAO.getInstance().get(1).getTorn());
			if(m.isTokio()) {
				System.out.println("AQUEST MONSTRE ESTÁ A TOKIO");
			}else {
				System.out.println("AQUEST MONSTRE NO ESTÁ A TOKIO");
			}
			System.out.println("TORN DE: "+m.getJugador().getNomJugador());
			System.out.println("Pulsa enter para tirar dados: ");
			
			sc.nextLine();
			
			MonstreDAO.getInstance().SolveRoll(m.getJugador(), m);
			System.out.println(m.toString());
			sc.nextLine();
			MonstreDAO.getInstance().ComprarCartesPoder(m);
			
			MonstreDAO.getInstance().asignarTorn(p);
		}
		
		
	}
	public static void SetAllPlayers() {
		int contador_jug = 0;
		boolean fi = false;
		while(!fi) {
			SetInfoJugadors();
			contador_jug++;
			if(contador_jug==4) {
				fi=true;
			}else if(contador_jug>=2) {
				System.out.println("Vols algún jugador més o amb "+contador_jug+" tens suficient? (1 començar partida / 2 crear un altre jugador)");
				int a = sc.nextInt();
				sc.nextLine();
				if(a==1) {
					fi=true;
				}else if(a!=2) {
					System.err.println("Pues ahora comienzo partida por listo, imbécil. Te crees muy listo por no hacerme caso? bueno pues monta y pedalea.");
				}
			}
		}
	}
	public static void SetInfoJugadors() {
		Jugador jugador = new Jugador();
		System.out.print("Posa el nom del Jugador: ");
		jugador.setNomJugador(sc.nextLine());
		System.out.println();
		System.out.print("Posa Cognom del jugador: ");
		jugador.setCognomJugador(sc.nextLine());
		JugadorDAO.getInstance().Insert(jugador);
		ArrayList<Monstre> monstres = MonstreDAO.getInstance().llistarMonstresVius(PartidaDAO.getInstance().get(1));
		ArrayList<Monstre> monstresDis = new ArrayList<Monstre>();
		int a = 0;
		for (Monstre monstre : monstres) {
			if(monstre.getJugador()==null) {
				System.out.println("Monstre "+monstre.getNomMonstre()+" disponible, pulsa "+a+" per seleccionarlo.");
				monstresDis.add(monstre);
			}
			a++;
		}
		while(jugador.getMonstres().size()==0) {
			int az = sc.nextInt();
			sc.nextLine();
			if(az>=0 && az<monstresDis.size()) {
				jugador.getMonstres().add(monstresDis.get(az));
				monstresDis.get(az).setJugador(jugador);
				MonstreDAO.getInstance().Update(monstresDis.get(az));
			}else {
				System.err.println("Tu eres tonto o que? Que pulses del 0 al "+(monstres.size()-1)+" imbécil.");
			}
		}
		JugadorDAO.getInstance().Update(jugador);
	}
	public static void SetInitalMonsters() {
		ArrayList<Monstre> arr = new ArrayList<>();
		Monstre monstre = new Monstre();
		monstre.setNomMonstre("M1");
		monstre.setNumeroVidesMonstre(10);
		monstre.setTokio(false);
		monstre.setEnergiaMonstre(0);
		monstre.setNumeroPuntsVictoriaMonstre(0);
		monstre.setEliminat(false);
		monstre.setJugable(true);
		arr.add(monstre);
		
		Monstre monstre2 = new Monstre();
		monstre2.setNomMonstre("M2");
		monstre2.setNumeroVidesMonstre(10);
		monstre2.setTokio(false);
		monstre2.setEnergiaMonstre(0);
		monstre2.setNumeroPuntsVictoriaMonstre(0);
		monstre2.setEliminat(false);
		monstre2.setJugable(true);
		arr.add(monstre2);
		
		Monstre monstre3 = new Monstre();
		monstre3.setNomMonstre("M3");
		monstre3.setNumeroVidesMonstre(10);
		monstre3.setTokio(false);
		monstre3.setEnergiaMonstre(0);
		monstre3.setNumeroPuntsVictoriaMonstre(0);
		monstre3.setEliminat(false);
		monstre3.setJugable(true);
		arr.add(monstre3);
		
		Monstre monstre4 = new Monstre();
		monstre4.setNomMonstre("M4");
		monstre4.setNumeroVidesMonstre(10);
		monstre4.setTokio(false);
		monstre4.setEnergiaMonstre(0);
		monstre4 .setNumeroPuntsVictoriaMonstre(0);
		monstre4.setEliminat(false);
		monstre4.setJugable(true);
		arr.add(monstre4);
		
		Monstre monstrePod = new Monstre();
		monstrePod.setJugable(false);
		monstrePod.setCostCartaPoder(0);
		monstrePod.setDescripcioCartaPoder("Carta de podeeeeeeeeeeer ");
		monstrePod.setNomCarta(CartesPoder.AlientoFuego);
		MonstreDAO.getInstance().Insert(monstrePod);
		
		MonstreDAO.getInstance().Insert(monstre);
		MonstreDAO.getInstance().Insert(monstre2);
		MonstreDAO.getInstance().Insert(monstre3);
		MonstreDAO.getInstance().Insert(monstre4);
		
		Partida partida = new Partida();
		partida.setNumeroJugadorsAssociats(3);
		PartidaDAO.getInstance().Insert(partida);
		
		
		partida.getMonstres().addAll(arr);
		partida.setTorn(1);
		monstre.setPartida(partida);
		monstre2.setPartida(partida);
		monstre3.setPartida(partida);
		monstre4.setPartida(partida);
		
		MonstreDAO.getInstance().Update(monstre);
		MonstreDAO.getInstance().Update(monstre2);
		MonstreDAO.getInstance().Update(monstre3);
		MonstreDAO.getInstance().Update(monstre4);
		
		PartidaDAO.getInstance().Update(partida);
	}
}
