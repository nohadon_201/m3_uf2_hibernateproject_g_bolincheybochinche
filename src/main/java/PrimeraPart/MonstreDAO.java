package PrimeraPart;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class MonstreDAO extends GenericDAO<Monstre, Integer> {
	private static MonstreDAO monstreDAO;

	private MonstreDAO() {

	}

	public static MonstreDAO getInstance() {
		if (monstreDAO == null) {
			monstreDAO = new MonstreDAO();
		}
		return monstreDAO;
	}

	public void AlientoFlamigero() {

	}

	public void Mimetismo() {

	}

	public void RayoReductor() {

	}

	public void EscupidorVeneno() {

	}
	
	public void UtilitzarCartaPoder(Monstre m) {
		if(m.getMonstreCarta()!=null){
			switch(m.getMonstreCarta().getNomCarta()) {
			case AlientoFuego:
				AlientoFlamigero();
				break;
			case EscupidorVeneno:
				EscupidorVeneno();
				break;
			case Mimetismo:
				Mimetismo();
				break;
			case RayoReductor:
				RayoReductor();
				break;
			}
		}else {
			System.err.println("El monstre no té una carta");
		}
	}
	
	public ArrayList<Monstre> LlistarCartesPoderLliures() {
		List<Monstre> list = this.findAll();
		ArrayList<Monstre> monstres = new ArrayList<>();
		for (Monstre monstre : list) {
			System.out.println(monstre.isJugable());
			if (!monstre.isJugable()) {
				monstres.add(monstre);
			}
		}
		return monstres;
	}

	public void ComprarCartesPoder(Monstre m) {
		Scanner sc = new Scanner(System.in);
		ArrayList<Monstre> arr = this.LlistarCartesPoderLliures();
		int ind = 0;
		int indCosesQuePotComprar = 0;
		for (Monstre monstre : arr) {
			ind++;
			if (m.getEnergiaMonstre() >= monstre.getCostCartaPoder()) {
				indCosesQuePotComprar++;
				System.out.println();
				System.out.println(
						"Pots Comprar: " + monstre.getNomCarta().toString() + " cost: " + monstre.getCostCartaPoder());
				System.out.println("Pulsa " + ind + " si vols comprarlo");
				System.out.println();
			}
		}
		System.out.println("Per sortir de la botiga pulsa 0");
		boolean fi = indCosesQuePotComprar == 0;
		while (!fi) {
			System.out.println("ENTRA");
			switch (sc.nextInt()) {
			case 1:
				if (arr.get(0).getCostCartaPoder() <= m.getEnergiaMonstre()) {
					m.setEnergiaMonstre(m.getEnergiaMonstre()-arr.get(0).getCostCartaPoder());
					arr.get(0).setMonstre(m);
					m.setMonstreCarta(arr.get(0));
					this.Update(m);
					this.Update(arr.get(0));
					fi=true;
				} else {
					System.err.println(
							"No tens suficient energía per comprar el monstre " + arr.get(0).getNomCarta().toString());
				}
				break;
			case 2:
				if (arr.get(1).getCostCartaPoder() <= m.getEnergiaMonstre()) {
					m.setEnergiaMonstre(m.getEnergiaMonstre()-arr.get(1).getCostCartaPoder());
					arr.get(1).setMonstre(m);
					m.setMonstreCarta(arr.get(1));
					this.Update(m);
					this.Update(arr.get(1));
					fi=true;
				} else {
					System.err.println(
							"No tens suficient energía per comprar el monstre " + arr.get(0).getNomCarta().toString());
				}
				break;
			case 3:
				if (arr.get(2).getCostCartaPoder() <= m.getEnergiaMonstre()) {
					m.setEnergiaMonstre(m.getEnergiaMonstre()-arr.get(2).getCostCartaPoder());
					arr.get(2).setMonstre(m);
					m.setMonstreCarta(arr.get(2));
					this.Update(m);
					this.Update(arr.get(2));
					fi=true;
				} else {
					System.err.println(
							"No tens suficient energía per comprar el monstre " + arr.get(0).getNomCarta().toString());
				}
				break;
			case 4:
				if (arr.get(3).getCostCartaPoder() <= m.getEnergiaMonstre()) {
					m.setEnergiaMonstre(m.getEnergiaMonstre()-arr.get(3).getCostCartaPoder());
					arr.get(3).setMonstre(m);
					m.setMonstreCarta(arr.get(3));
					this.Update(m);
					this.Update(arr.get(3));
					fi=true;
				} else {
					System.err.println(
							"No tens suficient energía per comprar el monstre " + arr.get(0).getNomCarta().toString());
				}
				break;
			default:
				fi = true;
				break;
			}
		}
	}

	/**
	 * This function check if there are monsters in tokio
	 * 
	 * @return true if there are monsters.
	 */
	public boolean hiHaMonstreTokio(Partida p) {
		System.out.println("aaaa");
		List<Monstre> monstres = llistarMonstresVius(p);
		System.out.println(monstres.size());
		for (Monstre monstre : monstres) {
			System.out.println("MONSTRE: " + monstre.getNomMonstre());
			if (monstre.isTokio()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * This functions provides a ArrayList with the monsters that are alive.
	 * 
	 * @return ArrayList with the monsters that are alive.
	 */
	public ArrayList<Monstre> llistarMonstresVius(Partida p) {
		ArrayList<Monstre> m = new ArrayList<Monstre>();
		List<Monstre> monstres = this.findAll();
		for (Monstre monstre : monstres) {
			if (!monstre.isEliminat() && monstre.getPartida()!=null && monstre.getPartida().getIdPartida() == p.getIdPartida()  ) {
				m.add(monstre);
			}
		}
		return m;
	}
	public  ArrayList<Monstre> llistarMonstresPartida(Partida p) {
		ArrayList<Monstre> m = new ArrayList<Monstre>();
		List<Monstre> monstres = this.findAll();
		for (Monstre monstre : monstres) {
			if (!monstre.isEliminat() && monstre.getPartida()!=null && monstre.getPartida().getIdPartida() == p.getIdPartida() && monstre.getJugador()!=null && monstre.isJugable() ) {
				m.add(monstre);
			}
		}
		return m;
	}

	/**
	 * This function set a random monster in Tokio
	 */
	public void setMonstreTokioAleatori(Partida p) {
		List<Monstre> monstres = llistarMonstresVius(p);
		for (Monstre monstre : monstres) {
			if (monstre.isTokio()) {
				monstre.setTokio(false);
				this.Update(monstre);
			}
		}
		Random r = new Random();
		Monstre m = monstres.get(r.nextInt(monstres.size()));
		m.setTokio(true);
		this.Update(m);
	}

	/**
	 * To find the list of monsters that contain the same PlayerID provided
	 * 
	 * @param jugador Player to search
	 * @return The list of monsters
	 */
	public ArrayList<Monstre> llistarMonstresJugador(Jugador jugador, Partida p) {
		ArrayList<Monstre> resultat = new ArrayList<Monstre>();
		List<Monstre> monstres = this.findAll();
		for (Monstre monstre : monstres) {
			if (monstre.getJugador() != null && monstre.getJugador().getIdJugador() == jugador.getIdJugador()
					&& monstre.getPartida().getIdPartida() == p.getIdPartida()) {
				resultat.add(monstre);
			}
		}
		return resultat;
	}

	/**
	 * the list of the monsters alive that are the contrincant of the monster
	 * provided
	 * 
	 * @param m the monster provided
	 * @return the array of the monsters that are the contrincant of the monster
	 *         provided
	 */
	public ArrayList<Monstre> ListMonstresViusContrincants(Monstre m, Partida p) {
		ArrayList<Monstre> resultat = new ArrayList<Monstre>();
		ArrayList<Monstre> monstres_vius = this.llistarMonstresVius(p);
		for (Monstre monstre : monstres_vius) {
			if (monstre.getIdMonstre() != m.getIdMonstre()) {
				resultat.add(monstre);
			}
		}
		return resultat;
	}

	/**
	 * If there are monster alive in tokio it will return it.
	 * 
	 * @return the monster in tokio.
	 */
	public Monstre getMonstreViuTokio(Partida p) {
		List<Monstre> monstres = llistarMonstresVius(p);
		for (Monstre monstre : monstres) {
			if (monstre.isTokio()) {
				return monstre;
			}
		}
		return null;
	}

	/**
	 * Update the monsters that have 0 lives or less.
	 */
	public void ActualitzarMonstresVius(Partida p) {
		List<Monstre> monstres = this.findAll();
		for (Monstre monstre : monstres) {
			if (!monstre.isEliminat() && monstre.getNumeroVidesMonstre() <= 0
					&& monstre.getPartida().getIdPartida() == p.getIdPartida()) {
				monstre.setEliminat(true);
				this.Update(monstre);
			}
		}
	}

	/**
	 * Assign the turn to the next monster
	 * 
	 * @param p
	 */
	public void asignarTorn(Partida p) {
		List<Monstre> list = llistarMonstresPartida(p);
		boolean b = false;
		int a = 0;
		for (Monstre monstre : list) {
			if (b) {
				p.setTorn(monstre.getIdMonstre());
				PartidaDAO.getInstance().Update(p);
				b = false;
				a++;
			} else if (p.getTorn() == monstre.getIdMonstre()) {
				b = true;
			}
		}
		if (a == 0) {
			p.setTorn(list.get(0).getIdMonstre());
			PartidaDAO.getInstance().Update(p);
		}
	}

	public void SolvePowerCarts(Monstre m) {
		ArrayList<Monstre> monstresPoderLliures = LlistarCartesPoderLliures();
		ArrayList<Monstre> monstresDisponibles = new ArrayList<Monstre>();
		for (Monstre monstre : monstresPoderLliures) {
			if(monstre.getCostCartaPoder()<=m.getEnergiaMonstre()) {
				monstresDisponibles.add(monstre);
			}
		}
		
	}

	/**
	 * the adventure begins, they are always beside you, your nerdy best friends and
	 * the DM to guide you.
	 * 
	 * @return the result of dices
	 */
	public ArrayList<Daus> Roll() {
		Random r = new Random();
		ArrayList<Daus> dausValues = new ArrayList<Daus>();
		for (int i = 0; i < 6; i++) {
			dausValues.add(Daus.values()[r.nextInt(5)]);
		}
		for (Daus daus : dausValues) {
			System.out.println("Has tret: "+daus.toString());
		}
		return dausValues;
	}

	/**
	 * Return the count of monsters that are alive.
	 * 
	 * @param partida
	 * @return
	 */
	public int CountMostresVius(Partida partida) {
		return llistarMonstresVius(partida).size();
	}

	/**
	 * Obté el monstre que més punts de victoria té.
	 * 
	 * @param partida
	 * @return
	 */

	public Monstre MonstreMaxPuntVictoria(Partida partida) {
		int maxVal = 0;
		Monstre monstre = null;
		for (Monstre m : llistarMonstresVius(partida)) {
			int val = m.getNumeroPuntsVictoriaMonstre();
			if (val > maxVal) {
				maxVal = val;
				monstre = m;
			}
		}
		return monstre;
	}

	/**
	 * Si només hi ha un monstre viu, el retorna.
	 * 
	 * @return
	 */

	public Monstre MonstreViu(Partida partida) {
		if (llistarMonstresPartida(partida).size() == 1) {
			return llistarMonstresVius(partida).get(0);
		} else {
			return null;
		}
	}

	/**
	 * Solve the results of the dice
	 * 
	 * @param j the player
	 * @param m the monster to edit the values
	 */
	public void SolveRoll(Jugador j, Monstre m) {
		ArrayList<Daus> resultats = Roll();
		LinkedHashMap<Daus, Integer> repeticions = new LinkedHashMap<>();
		for (Daus daus : resultats) {
			switch (daus) {
			case UN:
			case DOS:
			case TRES:
				repeticions.put(daus, repeticions.getOrDefault(daus, 0) + 1);
				break;
			case COR:
				m.setNumeroVidesMonstre(m.getNumeroVidesMonstre() + 1);
				break;
			case ENERGIA:
				m.setEnergiaMonstre(m.getEnergiaMonstre() + 1);
				break;
			case DANY:
				break;
			}
		}
		for (Map.Entry<Daus, Integer> entry : repeticions.entrySet()) {
			if (entry.getValue() >= 3) {
				switch (entry.getKey()) {
				case UN:
					m.setNumeroPuntsVictoriaMonstre(m.getNumeroPuntsVictoriaMonstre() + 1);
					m.setNumeroPuntsVictoriaMonstre(m.getNumeroPuntsVictoriaMonstre() + entry.getValue() - 3);
					break;
				case DOS:
					m.setNumeroPuntsVictoriaMonstre(m.getNumeroPuntsVictoriaMonstre() + 2);
					m.setNumeroPuntsVictoriaMonstre(m.getNumeroPuntsVictoriaMonstre() + entry.getValue() - 3);
					break;
				case TRES:
					m.setNumeroPuntsVictoriaMonstre(m.getNumeroPuntsVictoriaMonstre() + 3);
					m.setNumeroPuntsVictoriaMonstre(m.getNumeroPuntsVictoriaMonstre() + entry.getValue() - 3);
					break;
				}
			}
		}
		this.Update(m);
	}

}
